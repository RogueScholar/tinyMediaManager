#!/usr/bin/env bash
# ==============================================================================
# Filename:         tinyMediaManager.sh
# Description:      Bash script to launch the tinyMediaManager updater before
#                   launching the application jar file in Linux
# Project:          tinyMediaManager <https://www.tinymediamanager.org/>
# Author:           Manuel Laggner <manuel@tinymediamanager.org>
# Last updated:     2019-06-21
# GNU Bash Version: 5.0.3(1)-release
# ==============================================================================
# SPDX-License-Identifier: Apache-2.0
# Copyright © 2014, 2019 Manuel Laggner <manuel@tinymediamanager.org>.
#           © 2015, Myron Boyle <myron0815@gmx.net>.
#           © 2019, Peter J. Mello <admin@petermello.net>.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at:
#
#    https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
# ==============================================================================

# Explicitly set IFS to only newline and tab characters, eliminating errors
# caused by absolute paths where directory names contain spaces, etc.
IFS="$(printf '\n\t')"

# Display project name ASCII art for terminal invocations
base64 -d <<<"H4sIAHKYDV0AA61TMW7DQAzb8wUvt3kJqhbd+4L8IAJ4H8j/14ri6XJO2wAFcoARmy
IpSna26+fHrQHb9f3W/nGA00apefNSAz95Iu0O696OTSh9FK30E+tnOFXmcLVFk4ZN1T/P3TwZpHuJBD
2jT+cFaAcAc+LGWG7Ue45ge1R73udzsnItXn1sBzqZ6INUUJF/o5fzJOfNApj3+PWRS8HyXShiBXKt4x
6MqLbK6YyPl0HU/ZtjCbbQVbPFfQIXATSUU8Uaw41cmJd1cOmVC65K8lhJGc5TAItOPoc40rO2uE9ALW
SYxRkrIgZNBhahx9UDhX01dQqAvPG66UJSpGolkGoM8Ui31XkFgNxXGqbi9OdfzTxJLzvp96yh9vK6E9
8H+30DKgJluWsEAAA=" | gunzip

# Set desired execution flags
typeset -a PARAMS=(
	"-Djava.net.preferIPv4Stack=true"
	"-Dappbase=https://www.tinymediamanager.org/"
)

# Find the folder containing this script
script_path() {
  # Declare $dir is a local variable
  typeset dir
  # Use realpath to find the absolute path if called through a
  # symlink, else use the Bash built-in $BASH_SOURCE
  if [ -L "${BASH_SOURCE[0]}" ]; then
    dir="$(dirname "$(realpath -q "${BASH_SOURCE[0]}")")"
  else
    dir="$(dirname "${BASH_SOURCE[0]}")"
  fi
  # Strip any non-POSIX compliant control characters
  dir="$(echo "${dir}" | LC_ALL=POSIX tr -d '[:cntrl:]')"
  # Convert to Unicode (UTF-8)
  dir="$(echo "${dir}" | iconv -cs -f UTF-8 -t UTF-8)"
  # Print the result
  echo "${dir}"
}

# Make the script folder the working directory
typeset -r TMM_DIR="$(script_path)"
cd "$TMM_DIR" || { echo -e "\\tCould not traverse filesystem to the \
  installation directory. Please inspect the absolute path to the launch \
  script and remove any non-POSIX compliant control codes or characters."
  exit 1
}

# We're taking the long way around, so get a move-on already!!
java "${PARAMS[@]}" -jar getdown.jar .
