# Issue template

_Replace the space between brackets with an 'x' to check a box_

## Only submit bug reports for something that has happened to you more than once
_To solve an issue we almost always have to be able to reproduce it ourselves._

## Describe your environment

**What release channel are you on?**
- [ ] Release
- [ ] Pre-release
- [ ] Nightly
- [ ] Built from source

**What version number are you using?**
_Supply branch/tag/commit hash here if you built from source_


**Operating system**
- [ ] Windows
- [ ] MacOS
- [ ] Linux

My version of the above OS is: **Type your version here**


## Describe your experience

### This is what happened


### This is what I expected to happen


**Steps to reproduce:**
1. first
2. second
3. ...


## Provide files to help diagnose your issue

**Additional**
- [ ] Have you attached the logfile from the day it happened?
- [ ] Have you attached screenshots if you think they'll help us understand your issue?


**Do not type below this line; for developer use only**

/label ~user report
